import React from 'react';
import Button from '@atlaskit/button';
import { FieldTextStateless } from '@atlaskit/field-text';
import axios from 'axios';
import PropTypes from 'prop-types';
import EditIcon from '@atlaskit/icon/glyph/edit';
import RemoveIcon from '@atlaskit/icon/glyph/remove';
import styled from 'styled-components';

const TasksWrapper = styled.div`
  margin-top: 12px;
`;

const AddTaskButtonWrapper = styled.div`
  margin-top: 8px;
`;

const RowContainer = styled.form`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const StaticRowContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-top: 4px;
  padding-bottom: 3px;
  padding-left: 9px;
`;

const TextFieldWrapper = styled.div`
  margin-right: auto;
  width: 100%;
  padding-right: 16px;
  letter-spacing: normal;
`;

class TaskElement extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      textFieldValue: this.props.initialText,
      branchFieldValue: this.props.initialBranch
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.textFieldValue != nextProps.initialText) {
      this.setState(() => {
        return {
          textFieldValue: nextProps.initialText
        };
      })
    }
    if (this.state.branchFieldValue != nextProps.initialBranch) {
      this.setState(() => {
        return {
          branchFieldValue: nextProps.initialBranch
        };
      })
    }
  }

  handleEditTask = () => {
    this.props.onEdit(this.props.id);
  };

  handleDeleteTask = () => {
    this.props.onSave(this.props.id, null);
  };

  handleSaveTask = (e) => {
    e.preventDefault();
    const task = { text: this.state.textFieldValue, branch: this.state.branchFieldValue };
    this.props.onSave(this.props.id, task);
    console.dir('saving ' + task);
  };

  handleCancelEdit = () => {
    this.props.onCancel();
  };

  setTextFieldValue = e => this.setState({ textFieldValue: e.target.value });
  setBranchFieldValue = e => this.setState({ branchFieldValue: e.target.value });

  render() {
    return (
      <div>
        { !this.props.isEditing &&
          <StaticRowContainer>
            <TextFieldWrapper>
              {this.state.textFieldValue}
            </TextFieldWrapper>
            <TextFieldWrapper>
              {this.state.branchFieldValue}
            </TextFieldWrapper>
            <Button iconAfter={<EditIcon label="Edit Task"/>} appearance="subtle" onClick={this.handleEditTask} />
            <Button iconAfter={<RemoveIcon label="Delete Task"/>} appearance="subtle" onClick={this.handleDeleteTask} />
          </StaticRowContainer>
        }
        { this.props.isEditing &&
          <RowContainer onSubmit={this.handleSaveTask}>
            <TextFieldWrapper>
              <FieldTextStateless
                label="Task description"
                isLabelHidden={true}
                onChange={this.setTextFieldValue}
                value={this.state.textFieldValue}
                shouldFitContainer={true}
                placeholder='e.g. "Add tests"'
              />
            </TextFieldWrapper>
            <TextFieldWrapper>
              <FieldTextStateless
                label="Branch filter"
                isLabelHidden={true}
                onChange={this.setBranchFieldValue}
                value={this.state.branchFieldValue}
                shouldFitContainer={true}
                placeholder='e.g. "feature/*"'
              />
            </TextFieldWrapper>
            <Button onClick={this.handleSaveTask} appearance="primary">
              Save
            </Button>
            <Button onClick={this.handleCancelEdit} appearance="link">
              Cancel
            </Button>
          </RowContainer>
        }
      </div>

    )

  }

  static propTypes = {
    initialText: PropTypes.string,
    onEdit: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    isEditing: PropTypes.bool.isRequired
  }
}

export default class TasksConfig extends React.Component {

  pageToken = document.head.querySelector("[name=token]").content;
  repoId = document.head.querySelector("[name=repo-id]").content;
  tasks = document.body.querySelector("[name=initial-tasks]").content || '[]';

  constructor(props) {
    super(props);
    this.state = {
      tasksData: JSON.parse(this.tasks),
      taskBeingEdited: -1
    };
  }

  handleAddTask = () => {
    this.setState(() => {
      return {
        taskBeingEdited: this.state.tasksData.length
      }
    });
  };

  handleComponentEdit = (index) => {
    this.setState(() => {
      return {
        taskBeingEdited: index
      }
    })
  };

  handleComponentSave = (componentId, text) => {

    this.setState(() => {
      return {
        taskBeingEdited: -1
      }
    });

    let tasksToSave = this.state.tasksData.slice();

    if (!text) {
      tasksToSave.splice(componentId, 1);
    } else {
      tasksToSave[componentId] = text;
    }

    axios.put(`/update-task?jwt=${this.pageToken}&repoId=${this.repoId}`, {
      content: JSON.stringify(tasksToSave)
    })
    .then(() => {
      this.setState(function() {
        return {
          tasksData: tasksToSave.slice()
        }
      });
    })
    .catch((error) => {
      console.error(error);
    });
  };

  handleComponentCancel = () => {
    this.setState(() => {
      return {
        taskBeingEdited: -1
      }
    });

  };

  render() {
    return (
      <div>
        <TasksWrapper>
          {
            this.state.tasksData.map((task, index) => {
              return (
                <TaskElement
                  initialText={task.text}
                  initialBranch={task.branch}
                  id={index}
                  key={'task-' + index}
                  onEdit={this.handleComponentEdit}
                  onCancel={this.handleComponentCancel}
                  onSave={this.handleComponentSave}
                  isEditing={this.state.taskBeingEdited === index}
                />
              )
            })
          }
          {
            this.state.taskBeingEdited === this.state.tasksData.length &&
            <TaskElement
              initialText=""
              initialBranch=""
              id={this.state.tasksData.length}
              key={'task-' + this.state.tasksData.length}
              onEdit={this.handleComponentEdit}
              onCancel={this.handleComponentCancel}
              onSave={this.handleComponentSave}
              isEditing={true}
            />
          }
        </TasksWrapper>
        {
          this.state.taskBeingEdited === -1 && this.state.tasksData.length < 20 &&
          <AddTaskButtonWrapper>
            <Button appearance="link" onClick={this.handleAddTask}>
              + Add new task
            </Button>
          </AddTaskButtonWrapper>
        }
      </div>
    );
  }
}