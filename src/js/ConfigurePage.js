import React from "react";
import ReactDOM from "react-dom";
import "@atlaskit/css-reset";
import TasksConfig from "./TasksConfig";
import styled from 'styled-components';

const PageContainer = styled.div`
  width: 100%;
`;

window.onload = () => {
  ReactDOM.render(
    <PageContainer>
      <h3>Default tasks</h3>
      <p>
        Default tasks will automatically be added to any new pull request for this repository.
        Each task will need to be marked as complete before the pull request can be merged.
      </p>
      <TasksConfig />
    </PageContainer>,
    document.getElementById('root')
  );
};