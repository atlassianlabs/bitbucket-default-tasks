FROM node:5.7.0

run mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . /usr/src/app

run npm run build

run npm rebuild

EXPOSE 8080

CMD ["npm", "start"]