# Bitbucket Default Tasks

## NOTE: This app has been decomissioned, and is no longer being hosted by Atlassian. 
## As of May 2020 it still works, and can be started up and installed into a Bitbucket repository.
## The codebase has no active maintainer, so it might stop working in the future.





Bitbucket Default Tasks allows you to configure default tasks for your Bitbucket cloud repository.
If your repository has default tasks configured, they will be added to all new pull requests for that repository.
The tasks must be marked as complete before the pull request can be merged.

Bitbucket Default Tasks is implemented as an [Atlassian Connect app](https://developer.atlassian.com/cloud/bitbucket/integrating-with-bitbucket-cloud/),
built using the [Atlassian Connect for Node.js Express](https://bitbucket.org/atlassian/atlassian-connect-express) framework.

It uses [React](https://facebook.github.io/react/) and [AtlasKit](https://atlaskit.atlassian.com/)
in the frontend to style UI components according to the [Atlassian Design Guidelines](https://design.atlassian.com).

The app is not yet on the Atlassian marketplace, but you can install it manually in your Bitbucket cloud repository
using the app descriptor: https://bitbucket-default-tasks.dev.services.atlassian.com/atlassian-connect.json

## Starting the app for local development

1. Install [git], [node], [npm] \(2.7.5+) and [ngrok].
2. Run `npm install`.
3. Add your Bitbucket credentials to `credentials.json`.
4. Run `ngrok http 3000` and take note of the proxy's `https://..` base url.
5. Run `AC_LOCAL_BASE_URL=https://THE_NGROK_BASE_URL node app.js` from the
   repository root.

## Development loop

You can manually install/update/uninstall your add-ons from
`https://bitbucket.org/account/user/USERNAME/addon-management`.

Run `npm run watch` to automatically package your frontend code after any changes.
Changes to the backend code (`app.js` or `routes/index.js`) will require an app restart.
Changes to the app descriptor (`atlassian-connect.json`) will require a re-installation of the app in Bitbucket.

[git]: http://git-scm.com/
[node]: https://nodejs.org/
[npm]: https://github.com/npm/npm#super-easy-install
[ngrok]: https://ngrok.com/

## Tests

Coming soon...

## Contributors

Pull requests, issues and comments welcome. For pull requests:

- Add tests for new features and bug fixes
- Follow the existing style
- Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

- [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
- [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License

Copyright (c) 2017 Atlassian and others. Apache 2.0 licensed, see LICENSE.txt file.
